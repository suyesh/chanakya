class App
  def call(env)
    ["200", {"Content-Type" => "text/plain"}, ["Wait a minute. I'm the leader! I'm the one that says when we go."]]
  end
end

run App.new
